package com.base;

import com.base.config.AppConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Component
public class App {

    private ZippedFileSumTask zippedFileSumTask;
    private ScheduledExecutorService scheduledExecutorService;

    @Autowired
    public App(ZippedFileSumTask zippedFileSumTask, ScheduledExecutorService scheduledExecutorService) {
        this.zippedFileSumTask = zippedFileSumTask;
        this.scheduledExecutorService = scheduledExecutorService;
    }

    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
        App app = context.getBean(App.class);
        app.play();
    }

    public void play() {
        scheduledExecutorService.scheduleAtFixedRate(zippedFileSumTask, 5, 30, TimeUnit.SECONDS);
    }
}
