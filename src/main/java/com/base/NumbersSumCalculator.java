package com.base;

import com.base.model.InvalidFileException;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Objects;

public class NumbersSumCalculator {

    private final String  separator;

    public NumbersSumCalculator(String separator) {
        this.separator = Objects.requireNonNull(separator, "separator  must not be null");
    }

    public int calcSum(String fileName) {
        try {
            List<String> lines = Files.readAllLines(Paths.get(fileName));
            String[] numbers =  lines.get(0).split(separator);
            int sum = 0;
            for (String number :numbers) {
                sum  += Integer.valueOf(number.trim());
            }
            return sum;
        } catch (Exception e) {
            throw new InvalidFileException(e);
        }
    }
}
