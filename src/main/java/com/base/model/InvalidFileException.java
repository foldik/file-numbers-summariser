package com.base.model;

public class InvalidFileException extends RuntimeException {

    public InvalidFileException(String message) {
        super(message);
    }

    public InvalidFileException(Throwable cause) {
        super(cause);
    }
}
