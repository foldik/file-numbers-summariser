package com.base;

import com.base.model.InvalidFileException;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class SimpleFileSumTask implements Runnable {

    private static final DateTimeFormatter DATE_TIME_FORMAT = DateTimeFormatter.ofPattern("yyyyMMdd_hhmmss");

    private final String inputFolder;
    private final String inputFileExtension;
    private final String outputFileExtension;
    private final String archivedFileExtension;
    private final NumbersSumCalculator numbersSumCalculator;

    public SimpleFileSumTask(String inputFolder,
                             String inputFileExtension,
                             String outputFileExtension,
                             String archivedFileExtension,
                             NumbersSumCalculator numbersSumCalculator) {
        this.inputFolder = inputFolder;
        this.inputFileExtension = inputFileExtension;
        this.outputFileExtension = outputFileExtension;
        this.archivedFileExtension = archivedFileExtension;
        this.numbersSumCalculator = numbersSumCalculator;
    }

    @Override
    public void run() {
        try {
            Optional<String> fileName = getFileToProcess();
            if (fileName.isPresent()) {
                System.out.println("Processing: " + fileName.get());
                int sum = numbersSumCalculator.calcSum(fileName.get());
                Files.write(Paths.get(inputFolder, "result_" + now() + "." + outputFileExtension), contentToUtf8("Result " + sum));
                Files.move(Paths.get(fileName.get()), Paths.get(fileName.get() + "." + archivedFileExtension));
            } else {
                System.out.println("No file to process");
            }
        } catch (Exception e) {
            throw new InvalidFileException(e);
        }
    }

    private Optional<String> getFileToProcess() throws IOException {
        List<File> files = Files.list(Paths.get(inputFolder))
                .map(Path::toFile)
                .filter(f -> f.getName().endsWith("." + inputFileExtension))
                .collect(Collectors.toList());
        if (files.size() > 1) {
            throw new InvalidFileException("Only 1 file allowed to processed at the same time. Found " + files);
        } else if (files.size() == 1) {
            return Optional.of(files.get(0).getAbsolutePath());
        } else {
            return Optional.empty();
        }
    }

    private static String now() {
        return DATE_TIME_FORMAT.format(LocalDateTime.now());
    }

    private static byte[] contentToUtf8(String content) {
        return content.getBytes(StandardCharsets.UTF_8);
    }
}
