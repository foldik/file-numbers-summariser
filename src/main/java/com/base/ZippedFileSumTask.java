package com.base;

import com.base.model.InvalidFileException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class ZippedFileSumTask implements Runnable {

    private static final DateTimeFormatter DATE_TIME_FORMAT = DateTimeFormatter.ofPattern("yyyyMMdd_hhmmss");

    private final String inputFolder;
    private final String inputFileExtension;
    private final String outputFileExtension;
    private final String compressedFileExtension;
    private final NumbersSumCalculator numbersSumCalculator;

    public ZippedFileSumTask(String inputFolder,
                             String inputFileExtension,
                             String outputFileExtension,
                             String compressedFileExtension,
                             NumbersSumCalculator numbersSumCalculator) {
        this.inputFolder = inputFolder;
        this.inputFileExtension = inputFileExtension;
        this.outputFileExtension = outputFileExtension;
        this.compressedFileExtension = compressedFileExtension;
        this.numbersSumCalculator = numbersSumCalculator;
    }

    @Override
    public void run() {
        try {
            List<String> fileNames = getFileToProcess();
            if (!fileNames.isEmpty()) {
                int sum = calculateSum(fileNames);
                writeToResultFile(sum);
                zipFiles(fileNames);
                System.out.println("Processed " + fileNames);
            } else {
                System.out.println("No file was processed");
            }
        } catch (Exception e) {
            throw new InvalidFileException(e);
        }
    }

    private void zipFiles(List<String> fileNames) {
        try (ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(getZipFileName()))) {
            zos.setMethod(ZipEntry.DEFLATED);
            for (String fileName: fileNames) {
                ZipEntry zipEntry = new ZipEntry(new  File(fileName).getName());
                zos.putNextEntry(zipEntry);
                zos.write(Files.readAllBytes(Paths.get(fileName)));
                Files.delete(Paths.get(fileName));
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private String getZipFileName() {
        return Paths.get(inputFolder, "processed_" + now() + "." + compressedFileExtension).toFile().getAbsolutePath();
    }

    private void writeToResultFile(int sum) throws IOException {
        Files.write(Paths.get(inputFolder, "result_" + now() + "." + outputFileExtension), contentToUtf8("Result " + sum));
    }

    private int calculateSum(List<String> fileNames) {
        int sum = 0;
        for (String fileName : fileNames) {
            System.out.println("Processing: " + fileName);
            sum += numbersSumCalculator.calcSum(fileName);
        }
        return sum;
    }

    private List<String> getFileToProcess() throws IOException {
        return Files.list(Paths.get(inputFolder))
                .map(Path::toFile)
                .filter(f -> f.getName().endsWith("." + inputFileExtension))
                .map(File::getAbsolutePath)
                .collect(Collectors.toList());
    }

    private static String now() {
        return DATE_TIME_FORMAT.format(LocalDateTime.now());
    }

    private static byte[] contentToUtf8(String content) {
        return content.getBytes(StandardCharsets.UTF_8);
    }
}


