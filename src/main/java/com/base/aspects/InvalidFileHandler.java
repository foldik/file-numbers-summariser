package com.base.aspects;

import com.base.model.InvalidFileException;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Aspect
@Component
public class InvalidFileHandler {

    private static final DateTimeFormatter DATE_TIME_FORMAT = DateTimeFormatter.ofPattern("yyyyMMdd_hhmmss");

    @AfterThrowing(value = "execution(* com.base..*(..))", throwing = "ex")
    public void handleExcelption(InvalidFileException ex) throws Throwable {
        Files.write(Paths.get("error_cause_" + now() + ".txt"), messageToUtf8(ex));
        throw ex;
    }

    private static String now() {
        return DATE_TIME_FORMAT.format(LocalDateTime.now());
    }

    private static byte[] messageToUtf8(InvalidFileException ex) {
        return ex.getMessage().getBytes(StandardCharsets.UTF_8);
    }
}
