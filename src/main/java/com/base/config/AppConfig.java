package com.base.config;

import com.base.SimpleFileSumTask;
import com.base.NumbersSumCalculator;
import com.base.ZippedFileSumTask;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

@Configuration
@ComponentScan(basePackages = "com.base")
@PropertySource("classpath:application.properties")
@EnableAspectJAutoProxy(proxyTargetClass = true)
public class AppConfig {

    @Autowired
    private Environment environment;

    @Bean
    public NumbersSumCalculator numbersSumCalculator() {
        return new NumbersSumCalculator(environment.getRequiredProperty("numbers.separator"));
    }

    @Bean
    public ScheduledExecutorService scheduledExecutorService() {
        return Executors.newSingleThreadScheduledExecutor();
    }

    @Bean
    public SimpleFileSumTask fileSumTask() {
        return new SimpleFileSumTask(
                prop("input.folder"),
                prop("input.file.extension"),
                prop("output.file.extension"),
                prop("archived.file.extension"),
                numbersSumCalculator());
    }

    @Bean
    public ZippedFileSumTask zippedFileSumTask() {
        return new ZippedFileSumTask(
                prop("input.folder"),
                prop("input.file.extension"),
                prop("output.file.extension"),
                prop("compressed.file.extension"),
                numbersSumCalculator());
    }

    private String prop(String key) {
        return environment.getRequiredProperty(key);
    }

}
